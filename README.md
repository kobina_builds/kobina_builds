### "Who are you?"

I'm a mathematician who's decided to become a Machine Learning Engineer (MLE). 

### "Why did you choose to pursue this?" 

I got a PhD in Maths (Algebraic Geometry), decided that a career in academia wasn't for me, and that I'd rather make use of my analytical skills to build things that make me money by making even more money for other people.

### "Where do you come from?"
I am of Ghanaian, Sierra Leonean and British heritage, and beyond my personal professional goals, I would like to somehow apply my budding skills to contribute in some small way to the technological and economic advancement of Africa. 

### "AI is going to take all of our jobs. Why are you choosing to contribute to the problem?"
Machine learning models, like all technologies can be used for deeply beneficial and destructive purposes. **Currently, the world's gone a bit AI crazy, and people are looking to shoehorn machine learning into any and all software with minimal regard for the consequences. This is quite dangerous, because there is too much money being poured into making things, and not enough talk about what should and shouldn't be made. I hope we will become more circumspect in this regard, even if it affects the amount of work people like me get.**

#### The Destructive Side of Machine Learning
I believe that the coming years will show that machine learning applications have a greater potential to turn this world into a dystopian hellhole than any other technology with the exception of nuclear weapons. There is already long and recorded history of various proprietary programs being used to secretly surveil and exploit us, and these surveillance capabilities are definitely being supercharged by numerous machine learning microservices to profile and pigeonhole us in various ways. For this reason, I think that MLEs should be cognisant of the potentially destructive power that they wield when selecting projects and jobs. **I also think that free and open source (FOSS) machine learning development will very shortly become necessary for the privacy and security of all mankind.**

#### The Bright Side of Machine Learning
In the hands of skilled **and wise** engineers, these models can be used in countless ways to enhance our ability to make timely, and informed decisions. For instance, I dream of a world where we would have so much seismic and weather data (and such a deep understanding of it), that we will be able to predict the locations of catastrophic earthquakes and storms weeks ahead of time. 


### Ongoing Projects 
- 🔭 I’m currently working on:
   -  a Streamlit web application that predicts user demand for the City of Chicago's Divvy bike-sharing service.
   -  creating a service that identifies the genus (and eventually the species) of a mushroom. I'm using this project to force myself to transition to Pytorch and delve deeper into deep learning (I had used Tensorflow before, but I prefer Pytorch). 
   -  a language translator which is based on both pretrained transformers as well as one that I built from sratch using Pytorch.

### Current Learning Objectives
- Learn about more about various GANs, ConvNets, and transformers, and use them to make stuff.
- Start reading deep learning papers on a regular basis.
- Learn about model deployment on AWS and GCP.
   
👯 I’m open to collaborating with others on virtually any project, as I'm eager to learn about as many machine learning architectures and tools as possible.


### :hammer_and_wrench: Languages and Tools
  <img src="https://pluspng.com/img-png/python-logo-png-open-2000.png" title="Python" alt="Python" width="40" height="40"/>&nbsp;
    <img src="https://upload.wikimedia.org/wikipedia/commons/1/10/PyTorch_logo_icon.svg" title="Pytorch" alt="Pytorch" width="40" height="40"/>&nbsp; 
      <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Flogosdownload.com%2Flogo%2Fscikit-learn-logo-big.png&f=1&nofb=1&ipt=dc8109c7270108f1039f351c0c19e173c3f752eb44eb1b66c3559e7a6605ed06&ipo=images" title="scikit-learn" alt="scikit-learn" width="40" height="40"/>&nbsp; 
  <img src="https://avatars.githubusercontent.com/u/57251745?s=400&v=4" title="Optuna" alt="Optuna" width="40" height="40"/>&nbsp; 
  <img src="https://www.comet.com/images/logo_comet_light.png" title="CometML" alt="Comet" width="60" height="40"/>&nbsp; 
  <img src="https://uploads-ssl.webflow.com/618ceae2a430c960c6f6b19a/61a77bd7a2e4345dc9c999ba_Hopsworks%20Icon%20Green.png" title="Hopsworks" alt="Hopsworks" width="35" height="35"/>&nbsp; 
  <img src="https://vignette.wikia.nocookie.net/logopedia/images/0/04/Linux_logo.png/revision/latest?cb=20120814052336" title="Linux" alt="Linux" width="40" height="40"/>&nbsp;
</div>
